﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSwipes : MonoBehaviour
{
    public bool isSwipeLeft, isSwipeRight;
    private bool isMouseDown;

    public float unitsToDetect;

    public Vector2 mousePos;

    void Start()
    {
        mousePos = Input.mousePosition;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            isMouseDown = true;
            mousePos = Input.mousePosition;
        }
        if(Input.GetMouseButtonUp(0))
        {
            isMouseDown = false;
            isSwipeLeft = false;
            isSwipeRight = false;
        }

        if(isMouseDown == true)
        {
            if (mousePos.x + unitsToDetect > Input.mousePosition.x)
            {
                isSwipeLeft = true;
                isSwipeRight = false;
            }

            if (mousePos.x - unitsToDetect < Input.mousePosition.x)
            {
                isSwipeLeft = false;
                isSwipeRight = true;
            }
        }
    }


}
