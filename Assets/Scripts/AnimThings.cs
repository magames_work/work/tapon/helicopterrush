﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimThings : MonoBehaviour
{
    public GameObject bulletPref;
    public void Shoot()
    {
        Transform gun = GetComponent<Enemy>().gun;
        GameObject bullet = Instantiate(bulletPref, gun.position, gun.rotation, transform.GetChild(0).transform);
    }

    public void EnemyStartAiming()
    {
        GetComponent<Enemy>().isAiming = true;
    }

    public void EnemyStopAiming()
    {
        GetComponent<Enemy>().isAiming = false;
    }
}
