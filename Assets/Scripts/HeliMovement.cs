﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HeliMovement : MonoBehaviour
{
    public float speedFwd, speedSides;
    public CustomSwipes swipes;


    private GameObject heli;
    void Start()
    {
        heli = gameObject;
    }

    float x;
    Vector2 CatchScreenPos;
    void FixedUpdate()
    {
        //heli.transform.position += heli.transform.forward * speedFwd * Time.deltaTime;

        if(Input.GetMouseButtonDown(0))
        {
            CatchScreenPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
        if(Input.GetMouseButton(0))
        {
            // Vector2 screen = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            
            // float x = Mathf.Clamp(6.5f * screen.x - 2.5f, -4, 2.5f);


            Vector2 screen = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            Vector2 dif = screen - CatchScreenPos;

            //dif = dif * speedSides * Time.deltaTime;

            //if(heli.transform.position.x + dif.x < 8f & heli.transform.position.x + dif.x > -8f)
                heli.transform.Translate(new Vector3(dif.x * speedSides * Time.deltaTime, dif.y * speedFwd * Time.deltaTime ,0));
        }
        


        //if(swipes.isSwipeLeft)
        //{
        //    heli.transform.position -= heli.transform.right * speedSides * Time.deltaTime;
        //}

        //if (swipes.isSwipeRight)
        //{
        //    heli.transform.position += heli.transform.right * speedSides * Time.deltaTime;
        //}
    }
}
