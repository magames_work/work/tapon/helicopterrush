﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSlicerCollision : MonoBehaviour
{
    GameManager gm;
    UIManager ui;
    LvlManager lvl;
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        ui = FindObjectOfType<UIManager>();
        lvl = FindObjectOfType<LvlManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    GameObject slised;

    int id = 0;
    private void OnTriggerEnter(Collider other)
    {
        if(slised != other.gameObject & other.transform.GetComponentInParent<Enemy>() != null && other.transform.GetComponentInParent<Enemy>().id != id)
        {
            lvl.currentPoints++; 
            slised = other.gameObject;
            id = other.GetComponentInParent<Enemy>().id;
        }
    }

    void DisableWrong()
    {
        ui.wrong.SetActive(false);
    }
}
