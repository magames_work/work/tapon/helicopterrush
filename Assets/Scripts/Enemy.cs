﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    public int id;
    public Transform gun;
    public bool isAiming;
    public float timeToShoot;
    private float catchtimeToShoot;
    GameManager gm;
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        catchtimeToShoot = Time.time;
    }   

    // Update is called once per frame
    void Update()
    {
        if(isAiming)
        {
            transform.LookAt(gm.heli.transform.GetChild(0).transform);
            gun.LookAt(gm.heli.transform.GetChild(0).transform);
        }

        if(catchtimeToShoot + Random.Range(timeToShoot - 2, timeToShoot + 2) <= Time.time)
        {
            if(GetComponent<Animator>() != null)
                GetComponent<Animator>().SetTrigger("Shoot");
            catchtimeToShoot = Time.time;
        }

    }
}
