﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;

    public int health;
    public GameObject heli;

    void Start()
    {
        ui = GetComponent<UIManager>();
        lvl = GetComponent<LvlManager>();
        Application.targetFrameRate = 60;

        var enemyes = FindObjectsOfType<Enemy>();
        int id = 0;
        foreach(Enemy enemy in enemyes)
        {
            id++;
            enemy.id = id;
        }
    }

    void Update()
    {


        if(health <= 0)
        {
            print("game over");
        }
    }
}
