﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysOnY : MonoBehaviour
{
    public Transform heli;
    public float y;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(heli.position.y > y | heli.position.y < y)
            heli.position = new Vector3(heli.position.x, y, heli.position.z);
    }
}
